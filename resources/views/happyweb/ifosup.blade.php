@extends('layouts.portfolioTemplate')

@section('content')
    <h2>Réalisation d'un site internet pour l'école <strong>IFOSUP</strong> de Wavre</h2>
    <!-- Slideshow container -->
    <div class="slideshow-container">
        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/ifosupAccueil.jpg')}}" alt="Accueil Ifosup" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/ifosupConn.jpg')}}" alt="Connexion Ifosup" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/ifosupAdm.jpg')}}" alt="Panel admin Ifosup" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/ifosupCours.jpg')}}" alt="Cours Ifosup" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/ifosupMod.jpg')}}" alt="Module Ifosup" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/ifosupModifPass.jpg')}}" alt="Modification mot de passe Ifosup" style="width:100%">
        </div>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
    <div class="boutonRetour">
        <p><a href="{{route('index','#portfolio')}}"><button><i class="fas fa-arrow-left"></i>Retourner vers happyweb</button></a></p>
    </div>
@endsection