<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Mail\ContactMessageCreated;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use MercurySeries\Flashy\Flashy;


class PagesController extends Controller
{
    public function index(){
        return view('happyweb.index');
    }
    public function formulesClients(){
        return view('happyweb.formulesClients');
    }

    public function houyoux(){
        return view('happyweb.houyoux');
    }
    public function cvbellafionda(){
        return view('happyweb.cvbellafionda');
    }
    public function gildanails(){
        return view('happyweb.gildanails');
    }
    public function mdcarre(){
        return view('happyweb.mdcarre');
    }
    public function ifosup(){
        return view('happyweb.ifosup');
    }
    public function cvhouba(){
        return view('happyweb.cvhouba');
    }

    public function mentions(){
        return view('happyweb.mentionsLegales');
    }

    public function store(ContactRequest $request){

        $mailable= new ContactMessageCreated($request->prenom,$request->email,$request->tel,$request->sujetList,$request->sujet);

        Mail::to('contact@happyweb.be')->send($mailable);

        Flashy::message('Votre message à bien été envoyé ! Je vous recontacterai au plus vite !', 'http://your-awesome-link.com');

        return redirect()->route('index');
    }

}
