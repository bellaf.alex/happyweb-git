<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mentions Légales</title>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/reset.css') }}">
</head>
<body>
@if(app()->getLocale() == 'fr')
    <div id="mentionsLegales">

        <div id="fond_mentions_legales">

            <h2>Droit d'auteur</h2>
            <p>Le site www.happyweb.be constitue une création protégée par le droit d'auteur. Les textes, photos et autres éléments de notre site sont protégés par le droit d'auteur. Toute copie, adaptation, traduction, arrangement, communication au public, location et autre exploitation, modification de tout ou partie de ce site sous quelle que forme et par quel que moyen que ce soit, électronique, mécanique ou autre, réalisée dans un but lucratif ou dans un cadre privé, est strictement interdit sans mon autorisation préalable .Toute infraction à ces droits entrainera des poursuites civiles ou pénales.</p>

            <h2>Marques et noms commerciaux</h2>
            <p>Les dénominations, logos et autres signes utilisés sur notre site sont des marques et/ou noms commerciaux légalement protégés. Tout usage de ceux-ci ou de signes ressemblants est strictement interdit sans un accord préalable et écrit.</p>

            <h2>Autres informations</h2>

{{--            <p><a href="https://fontawesome.com/" target="_blank" rel="noopener"`>Les icônes sont sous licence Creative Commons Attribution 4.0 International license</a></p>--}}
            <p>A l'exception des photos présentées dans la section <b>"Qui sommes nous ?"</b>,<b>"Nos réalisations"</b> et <b>"Nos logos"</b>, les images sont libres de droit et proviennent du site <a href="https://pixabay.com/fr/" target="_blank" rel="noopener">Pixabay</a></p>
            <p><b>Provenance des icones (libre de droit):</b></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/settings-3">Automation icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/test-passed">Test Passed icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/google-calendar">Google Calendar icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/rocket">Rocket icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/wardrobe">Wardrobe icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/baby-footprints-path">Baby Footprint icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/light-on">Light On icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/hourglass-sand-top">Sand Watch icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/right">Right icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>

            <h2>Coordonnées</h2>
            <p>Vous pouvez nous contacter par Email à <a href="mailto:contact@happyweb.be"> contact@happyweb.be</a></p>
{{--            <a href="{{route('store','#prenom')}}"></a>--}}
            <h2>Hébergeur</h2>
            <p>OVH</p>
            <p>2 rue Kellermann</p>
            <p>59100 Roubaix - France. </p>
            <p>www.ovh.com</p>

            <h2>Cookies</h2>
            <h3>Définition d'un cookie</h3>
            <p>Un cookie est un fichier texte déposé sur le disque dur de votre ordinateur, de votre appareil mobile ou de votre tablette lors de la visite d'un site ou de la consultation d'une publicité. Il a pour but de collecter des informations relatives à votre navigation et de vous adresser des services adaptés à votre terminal.
                Le cookie contient un code unique permettant de reconnaître votre navigateur lors de votre visite sur le site web ou lors de futures visites répétées. Les cookies peuvent être placés par le serveur du site web que vous visitez ou par des partenaires avec lesquels ce site web collabore. Le serveur d’un site web peut uniquement lire les cookies qu’il a lui-même placés et n’a accès à aucune autre information se trouvant sur votre ordinateur ou sur votre appareil mobile.
                Les cookies assurent une interaction généralement plus aisée et plus rapide entre le visiteur et le site web. En effet, ils mémorisent vos préférences (la langue choisie ou un format de lecture par exemple) et vous permettent ainsi d’accélérer vos accès ultérieurs au site et de faciliter vos visites.
                De plus, ils vous aident à naviguer entre les différentes parties du site web. Les cookies peuvent également être utilisés pour rendre le contenu d’un site web ou la publicité présente plus adaptés aux choix, goûts personnels et aux besoins du visiteur.
                Les informations ainsi récoltées sont anonymes et ne permettent pas votre identification en tant que personne. En effet, les informations liées aux cookies ne peuvent pas être associées à un nom et/ou prénom parce qu’elles ne contiennent pas de données à caractère personnel.
                Les cookies sont gérés par votre navigateur internet. L’utilisation des cookies nécessite votre consentement préalable et explicite. Vous pourrez toujours revenir ultérieurement sur celui-ci et refuser ces cookies et/ou les supprimer à tout moment, en modifiant les paramètres de votre navigateur.</p>


            <h2>Autoriser ou bloquer les cookies ?</h2>
            <p>La plupart des navigateurs internet sont automatiquement configurés pour accepter les cookies. Cependant, vous pouvez configurer votre navigateur afin d’accepter ou de bloquer les cookies.
                Je ne peux cependant vous garantir l’accès à tous les services de mon site internet en cas de refus d’enregistrement de cookies.</p>


            <a class="myButton2" href="{{route('index')}}">Retourner sur le site</a>
        </div>
    </div>
@endif

@if(app()->getLocale() == 'en')
    <div id="mentionsLegales">
        <div id="fond_mentions_legales">
            <h2>Copyright</h2>
            <p>The site www.davidhouba-dev.be is a protected creation by copyright. Texts, pictures and others elements are protected by copyright. Any copy, adaptation, translation, arrangement, communication to the public, location and others exploitation, modification of all or part of this site under wich way and by any means, electronic, mechanical or other, carried out for profit or in a private setting, is strictly forbidden without my preliminary authorisation. any infringement of these rights will lead to civil or criminal prosecution.</p>

            <h2>Trademarks and trade names</h2>
            <p>The denominations, logos and others signs used on my website are trademarks and/or trade names legally protected. All use of these or similar signs is strictly forbidden without a prior written agreement.</p>

            <h2>Other informations</h2>

            <p><a href="https://fontawesome.com/" target="_blank" rel="noopener"`>Icons are under Creative Commons Attribution 4.0 International license</a></p>
            <p><a href="https://pixabay.com/fr/" target="_blank" rel="noopener">At the exception of the photo presented in the "About" section, the images are free of copyright and come from the Pixabay site</a></p>

{{--            <div>Flags icons are made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>--}}
{{--            <div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>--}}
{{--            <div>Icons made by <a href="https://www.flaticon.com/authors/surang" title="surang">surang</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>--}}
{{--            <div>Icons made by <a href="https://www.flaticon.com/authors/popcorns-arts" title="Icon Pond">Icon Pond</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>--}}
{{--            <div>Icons made by <a href="https://www.flaticon.com/authors/flat-icons" title="Flat Icons">Flat Icons</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>--}}

            <p><a target="_blank" href="https://icons8.com/icons/set/settings-3">Automation icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/test-passed">Test Passed icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/google-calendar">Google Calendar icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/rocket">Rocket icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/wardrobe">Wardrobe icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/baby-footprints-path">Baby Footprint icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/light-on">Light On icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/hourglass-sand-top">Sand Watch icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>
            <p><a target="_blank" href="https://icons8.com/icons/set/right">Right icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></p>

            <h2>Contact information</h2>
            <h3>Owner / creator of the site</h3>
            <p>You can contact me by Email at davidhouba.dev@gmail.com</p>
{{--            <a href="{{route('store','#prenom')}}"></a>--}}

            <h2>Web Host</h2>
            <p>OVH</p>
            <p>2 rue Kellermann</p>
            <p>59100 Roubaix - France. </p>
            <p>www.ovh.com</p>

            <h2>Cookies</h2>
            <h3>Definition of a cookie</h3>
            <p>A cookie is a text file deposited on your computer's hard drive, your mobile device or your pad when visiting a site or during the consultation of an advertisement. His goals is to cllect informations relating to your navigation and to sent you adapted services.
                The cookie contains a unique code to recognize your rowser during your visit on the website or during future visits. Cookies can be placed by the server of the website you are visiting or by partners with whom this website collaborate. A server of a website can only read cookies that he has himself placed and does not have access to any other information on your computer or on your mobile device.
                Cookies provide generally an easier and faster interaction between the visitor and the website. Indeed, they memorise your preferences (the chosen language or a reading format for example) and allow you to speed up your subsequent access to the site and to facilitate your visits.
                Furthermore, they help you to navigate between the different parts of the website. Cookies can also be used to make the content of a website or the advertisement presents more adapted to the choixe, personnal taste and to the needs of the visitor.
                the informations thus collected are anonymous and does not allow your identification as a person. Indeed, the cookies informations cannot be associated to a lastname and/or firstnale because they does not contain any personnal data.
                Cookies are managed by your web browser. The use of cookies requires your prior consent. You can always came back on this one and denied those cookies and/or delete them at any moment, by changing the settings of your web browser.</p>

            <h2>Allow or block cookies?</h2>
            <p>Most of web browsers are automatically configured to accept cookies. However, you can configure your web browser to accept or block cookies.</p>
            <a class="myButton2" href="{{route('index')}}">Back to the site</a>
        </div>
    </div>

@endif


</body>
</html>
