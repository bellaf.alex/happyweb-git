<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/reset.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/images/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/images/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('/images/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('/images/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">
    <meta name="Description" content="Chez Happy Web, nous réalisons votre site, pour vous et avec vous ! Nous mettons nos connaissances à votre service pour vous fournir le site dont vous rêvez !">
    <script src="https://kit.fontawesome.com/35d612b19a.js"></script>
    <title>Happy Web | Agence Web</title>
</head>
<body>

@include('layouts/partials/_nav')

<main class="container">
    @yield('content')
</main>

@include('layouts/partials/_footer')

<script src="{{ asset('/js/menuHamb.js') }}"></script>
<script src="{{ asset('/js/smooth.js') }}"></script>

@include('flashy::message')

<script src="{{asset('/js/cookiechoices.js')}}"></script><script>document.addEventListener('DOMContentLoaded', function(event){cookieChoices.showCookieConsentBar('Ce site utilise des cookies pour vous offrir le meilleur service. En poursuivant votre navigation, vous acceptez l’utilisation des cookies.', 'J’accepte', 'En savoir plus', 'https://happyweb.be/mentionsLegales');});</script>
</body>
</html>
