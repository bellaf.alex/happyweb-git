<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('/','PagesController@index')->name('index');
Route::get('/formulesClients','PagesController@formulesClients')->name('formulesClients');
Route::get('/houyoux','PagesController@houyoux')->name('houyoux');
Route::get('/cvbellafionda','PagesController@cvbellafionda')->name('cvbellafionda');
Route::get('/gildanails','PagesController@gildanails')->name('gildanails');
Route::get('/mdcarre','PagesController@mdcarre')->name('mdcarre');
Route::get('/ifosup','PagesController@ifosup')->name('ifosup');
Route::get('/cvhouba','PagesController@cvhouba')->name('cvhouba');
Route::get('/mentionsLegales','PagesController@mentions')->name('mentionsLegales');

Route::post('/','PagesController@store')->name('store');



