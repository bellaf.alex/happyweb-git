@component('mail::message')
    # Hey Admin

    - {{$prenom}}
    - {{$email}}
    - {{$tel}}
    - {{$sujetList}}

    @component('mail::panel')
        {{$sujet}}
    @endcomponent

    Thanks,<br>
    {{config('app.name')}}
@endcomponent

