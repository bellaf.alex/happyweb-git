@extends('layouts.template')

@section('content')

    <header id="logo">
        <div class="header">
            <div class="video_header">
                 <video width="100%"  autoplay="" loop  muted>
                    <source src="{{ asset('/video/vidHeader1080.mp4') }}" type="video/mp4">
                </video>
            </div>
            <figure class="logoPanda"><img src="{{ asset('/images/logoMenu.png') }} " alt="logo Menu"></figure>
            <h1><strong>Happy</strong><br> <span class="web"><strong>Web</strong></span></h1>
            <a href="#intro"><figure class="arrow"><img class="heartBeat" src="{{ asset('/images/flecheDown.png') }} " alt="logo Menu"></figure></a>

        </div>
    </header>

    <div id="wrapper">
        <div id="intro">
            <p>
                Chez <strong><span>Happy Web</span></strong>, nous réalisons votre <strong>site</strong>, <span>pour vous</span> et <span>avec vous</span> !
                Nous mettons nos connaissances à votre service pour vous fournir le
                <strong>site</strong> dont vous rêvez !
            </p>
        </div>

        <h2>"Pour quels <strong>besoins</strong> ?"</h2>
        <div id="besoins">

            <div id="besoins__relooking">
                <img src="{{ asset('/images/armoire.png') }} " alt="armoire icone">
                <h3>Un relooking ?</h3>
                <p>
                    Vous voulez donnez un coup de jeune à votre <strong>site</strong> ?
                    Nous vous y aidons !
                </p>
            </div>

            <div id="questions__premierPas">
                <img src="{{ asset('/images/premierPas.png') }} " alt="steps icone">
                <h3>Vos premiers pas ?</h3>
                <p>
                    Vous voulez avoir votre <strong>site web</strong> mais vous ne savez pas comment faire ? Nous réalisons avec vous votre <strong>site</strong> de A à Z !
                </p>
            </div>

            <div id="questions__lumière">
                <img src="{{ asset('/images/light.png') }} " alt="light icone">
                <h3>Vous cherchez la lumière ?</h3>
                <p>
                    Nous faisons en sorte que votre <strong>site</strong> soit <strong>visible</strong>
                    dès les premières recherches.
                </p>
            </div>

            <div id="questions__temps">
                <img src="{{ asset('/images/time.png') }} " alt="time icone">
                <h3>Pas de temps à perdre ?</h3>
                <p>
                    Nous utilisons des outils modernes et efficaces
                    pour répondre à votre demande.
                </p>
            </div>

        </div>

        <h2 >"Comment ?"</h2>

    <div id="questions">

        <div id="questions__etape1">
            <img src="{{ asset('/images/calendar.png') }} " alt="calendar icone">
            <h3>Premier contact</h3>
                <p>
                    Lors d'un premier rendez-vous, nous évaluons vos <strong>besoins</strong> et envies.
                </p>
        </div>

        <div id="questions__etape2">
            <img src="{{ asset('/images/gear.png') }} " alt="gear icone">
            <h3>Notre réflexion</h3>
                <p>
                    Afin d'avoir une vision claire et précise du <strong>projet</strong>, nous vous proposons un
                    <strong>devis</strong> précis et vous présentons une représentation graphique de votre <strong>futur site</strong>.
                </p>
        </div>

        <div id="questions__etape3">
            <img src="{{ asset('/images/approval.png') }} " alt="approval icone">
            <h3>Votre approbation</h3>

                <p>
                    Nous réglons avec vous les derniers détails...
                    On ne démarre pas avant votre top départ !
                </p>
        </div>

        <div id="questions__etape4">
            <img src="{{ asset('/images/rocket.png') }} " alt="start icone">
            <h3>La réalisation</h3>
            <p>
                C'est parti ! La construction du <strong>site</strong> commence...
                On se retrouve sur la ligne d'arrivée !
            </p>
        </div>

    </div>



        <div class="banniereOffres" id="banniereOffres">
            <video width="100%"  autoplay="" loop  muted>
                <source src="{{ asset('/video/vidBanniereBlue-min.mp4') }}" type="video/mp4">
            </video>
            <h2 id="formules">Nos offres</h2>
            <p>Envie d'en savoir plus ? N'hésitez pas à aller voir nos différentes formules !</p>
            <a href="{{route('formulesClients')}}">Consultez nos formules</a>
        </div>

<h2 id="portfolio">Nos créations</h2>
    <h3>Nos sites réalisés</h3>
        <div id="creations">
            <div id="site__alexane2" class="crea" >
                <a href="{{route('houyoux')}}"><img src="{{ asset('/images/site__alexane2.jpg') }}" alt="Site Dr.E.Hoyoux"><span><strong>Réalisation</strong> d'un <strong>site internet</strong> pour le <strong>Dr.Emmanuelle Houyoux</strong>.</span></a>

            </div>

            <div class="column">
                <div id="site__alexane1" class="crea">
                <a href="{{route('cvbellafionda')}}"><img src="{{ asset('/images/site__alexane1.png') }}" alt="site Cv en ligne Alexane Bella Fionda"><span><strong>Réalisation</strong> d'un <strong>curriculum vitae</strong> en ligne de <strong>Alexane Bella Fionda</strong>.</span></a>

                </div>

                <div id="site__alexane3" class="crea">
                <a href="{{route('gildanails')}}"><img src="{{ asset('/images/site__alexane3-min.jpg') }}" alt="site Gilda Nails"><span><strong>Réalisation</strong> d'un <strong>site internet</strong> pour l'<strong>entreprise</strong> "<strong>Gilda Nails</strong>".</span></a>

                </div>
            </div>

            <div class="column">
                <div id="mdCarre__all" class="crea">
                <a href="{{route('mdcarre')}}"><img id="mdCarre" src="{{ asset('/images/site__david2.jpg') }}" alt="Site MdCarre"><span><strong>Réalisation</strong> d'un <strong>site internet</strong> pour l'<strong>entreprise</strong> "<strong>Mdcarre</strong>".</span></a>

                </div>

                <div id="site__ifosup" class="crea">
                <a href="{{route('ifosup')}}"> <img src="{{ asset('/images/ifosup.jpg') }}" alt="Site Ifosup"><span><strong>Réalisation</strong> d'un <strong>site internet</strong> pour l'école "<strong>IFOSUP</strong> de Wavre".</span></a>

                </div>

                <div id="site__david1" class="crea">
                <a href="{{route('cvhouba')}}"><img src="{{ asset('/images/site__david1.png') }}" alt="site Cv en ligne David Houba"><span><strong>Réalisation</strong> d'un <strong>curriculum vitae</strong> en ligne de <strong>David Houba</strong>.</span></a>

            </div>
            </div>
        </div>
    <h3>Nos logos</h3>
    <div class="logo">
        <figure>
            <img src="{{asset('images/coeur.svg')}}" alt="Logo coeur">
        </figure>
        <figure class="lotus">
            <img src="{{asset('images/lotus.svg')}}" alt="Logo lotus">
        </figure>
        <figure>
            <img src="{{asset('images/dauphin.svg')}}" alt="Logo dauphin">
        </figure>
        <figure>
            <img src="{{asset('images/LogoBio.svg')}}" alt="Logo bio">
        </figure>
        <figure>
            <img src="{{asset('images/LogoRose.svg')}}" alt="Logo rose">
        </figure>
        <figure >
            <img src="{{asset('images/LogoFrite.svg')}}" alt="Logo Frite">
        </figure>
        <figure>
            <img src="{{asset('images/fancyHats.svg')}}" alt="Logo fancyHats">
        </figure>
        <figure>
            <img src="{{asset('images/infinity2.jpg')}}" alt="Logo infinity">
        </figure>
        <figure>
            <img src="{{asset('images/sphere.jpg')}}" alt="Logo sphere">
        </figure>
    </div>
<h2 id="equipe">Qui sommes-nous ?</h2>

        <div id="quiSommesNous">

            <div id="avatarAlex">
                <img src="{{ asset('/images/myAvatar(Alexane).png') }}" alt="Avatar Alexane">
            </div>

            <div id="avatarDav">
                <img src="{{ asset('/images/myAvatar(200x200).png') }}" alt="Avatar David">
            </div>

        </div>

        <div id="presentation">
            <p>
                Nous sommes deux passionnés de l'<strong>informatique</strong> et après plusieurs années de formations intensives, nous avons décidé de nous lancer dans le grand bain
                en créant notre propre <strong>Agence Web</strong>.
            </p>
            <p>
                Notre duo d'enfer adore relever les défis tout en restant <strong>"happy"</strong> et cela en toute circonstance !
                Cela fait maintenant quelques temps que nous travaillons ensemble et ... ça fonctionne !
            </p>
            <p>
                Fort d'une vision commune, nous réaliserons chaque <strong>projet</strong> avec la même passion et l'envie de prendre soin de réaliser vos rêves avec succès !
            </p>
        </div>

<h2 id="contact">Contactez-nous</h2>


<div id="fond__contact">

    <div id="contact_wrapper">

        <form id="FormContact" action="{{route('store','#prenom')}}" method="POST" novalidate>

            @csrf
            <label for="prenom"> Nom et prénom</label>
            {!! $errors->first('prenom','<div class="is-invalid">:message</div>') !!}
            <input type="text" id="prenom" name="prenom" value="" placeholder="Votre nom et prénom" required>

            <label for="email">Email</label>
            {!! $errors->first('email','<div class="is-invalid">:message</div>') !!}
            <input type="email" id="email" name="email" value="" placeholder="ex:dupont@gmail.com" required>

            <label for="tel">Télephone</label>
            {!! $errors->first('tel','<div class="is-invalid">:message</div>') !!}
            <input type="tel" id="tel" name="tel" value="" placeholder="ex:0478/85/98/98">

            <label for="sujetList" id="center">Sujet<i class="fas fa-caret-down"></i></label>
            {!! $errors->first('sujet','<div class="is-invalid">:message</div>') !!}
            <select id="sujetList" name="sujetList">
                <option value="rdv">Prendre un rendez-vous</option>
                <option value="questions">J'ai une question</option>
                <option value="autre">Autre</option>
            </select>

            <label for="sujet">C'est à vous !</label>
            <textarea id="sujet" name="sujet" placeholder="Votre message ... " style="height:200px">{{old('sujet')}}</textarea>

            <input type="submit" name='contactSubmit' value="Envoyer">

        </form>

    </div>
    <ul class="social">
        <li><a href="https://www.facebook.com/pg/HappyWeb-111360673971429" target="_blank" rel="noopener"><i class="fab fa-facebook-square"></i></a></li>
        <li class="twitter"><a href="https://twitter.com/HappyWeb_" target="_blank" rel="noopener"><i class="fab fa-twitter-square"></i></a></li>
        <li><a href="https://www.linkedin.com/company/happy-web-belgium/" target="_blank" rel="noopener"><i class="fab fa-linkedin"></i></a></li>
    </ul>

</div>

    </div>
    @endsection
