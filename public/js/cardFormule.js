var faceAvant = document.querySelector('.bronzeAvant')
var faceArriere = document.querySelector('.bronzeArriere')

faceAvant.addEventListener('click',function(){
    faceAvant.classList.add('faceAvant')
    faceArriere.classList.add('faceArriere')
    faceAvant.classList.remove('clickAvant')
    faceArriere.classList.remove('clickArriere')
})

faceArriere.addEventListener('click',function(){
    faceArriere.classList.remove('faceArriere')
    faceAvant.classList.add('clickAvant')
    faceArriere.classList.add('clickArriere')
    faceAvant.classList.remove('faceAvant')
})

var faceAvantSilver = document.querySelector('.silverAvant')
var faceArriereSilver = document.querySelector('.silverArriere')

faceAvantSilver.addEventListener('click',function(){
    faceAvantSilver.classList.add('faceAvant')
    faceArriereSilver.classList.add('faceArriere')
    faceAvantSilver.classList.remove('clickAvant')
    faceArriereSilver.classList.remove('clickArriere')
})

faceArriereSilver.addEventListener('click',function(){
    faceArriereSilver.classList.remove('faceArriere')
    faceAvantSilver.classList.add('clickAvant')
    faceArriereSilver.classList.add('clickArriere')
    faceAvantSilver.classList.remove('faceAvant')
})

var faceAvantGold = document.querySelector('.goldAvant')
var faceArriereGold = document.querySelector('.goldArriere')

faceAvantGold.addEventListener('click',function(){
    faceAvantGold.classList.add('faceAvant')
    faceArriereGold.classList.add('faceArriere')
    faceAvantGold.classList.remove('clickAvant')
    faceArriereGold.classList.remove('clickArriere')
})

faceArriereGold.addEventListener('click',function(){
    faceArriereGold.classList.remove('faceArriere')
    faceAvantGold.classList.add('clickAvant')
    faceArriereGold.classList.add('clickArriere')
    faceAvantGold.classList.remove('faceAvant')
})