<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/reset.css') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/images/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/images/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('/images/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('/images/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">
    <script src="https://kit.fontawesome.com/35d612b19a.js"></script>
    <title>Happy Web | Nos créations</title>
</head>
<body>
    <div class="portfolio">
        {{-- <div class="return"><a href="{{route('index','#portfolio')}}"><i class="fas fa-arrow-left"></i></a></div> --}}
        <div class="head"><div class="logo"><a href="{{route('index','#portfolio')}}"><img src="{{ asset('/images/miniLogo.png') }}" alt="Logo"></a></div></div>
        
        <main class="content">
            @yield('content')
        </main>
        
        @include('layouts/partials/_footer')
    </div>
    <script src="{{ asset('/js/slider.js') }}"></script>
</body>
</html>