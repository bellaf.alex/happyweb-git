<div class="iconesHamb"><i class="fas fa-bars"></i></div>
<nav id="nav" class="cacher invisible">
    <div class="close"><i class="fas fa-times"></i></div>
    <a href="#logo" class="panda"><img src="{{ asset('/images/miniLogo.png') }}" alt="logo Menu"></a>
    <ul class="nav">
        <li><a href="#intro" class="link">Accueil</a></li>
        <li><a href="{{route('formulesClients')}}" class="link2">Nos offres</a></li>
        <li><a href="#portfolio" class="link3">Nos créations</a></li>
        <li><a href="#equipe" class="link4">Qui sommes nous ?</a></li>
        <li><a href="#contact" class="link5">Contact</a></li>
    </ul>
    <ul class="resSociaux">
        <li><a href="https://www.facebook.com/pg/HappyWeb-111360673971429" target="_blank" rel="noopener"><i class="fab fa-facebook-square"></i></a></li>
        <li><a href="https://twitter.com/HappyWeb_" target="_blank" rel="noopener"><i class="fab fa-twitter-square"></i></a></li>
        <li><a href="https://www.linkedin.com/company/happy-web-belgium/" target="_blank" rel="noopener"><i class="fab fa-linkedin"></i></a></li>
    </ul>
</nav>


