@extends('layouts.portfolioTemplate')

@section('content')
    <h2>Réalisation du curriculum vitae en ligne de Alexane Bella Fionda</h2>
    <!-- Slideshow container -->
    <div class="slideshow-container">
        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/accueilBellaFionda.png')}}" alt="Accueil Cv en ligne Bella Fionda" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/infoUtilesBellaFionda.png')}}" alt="Informations utiles" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/experiencesBellaFionda.png')}}" alt="Expériences Bella Fionda" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/contactBellaFionda.png')}}" alt="Contact Bella Fionda" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/accueilPortfolioBellaFionda.png')}}" alt="Accueil portfolio Bella Fionda" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/portfolioBellaFionda.png')}}" alt="Portfolio Bella Fionda" style="width:100%">
        </div>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
    <div class="boutonRetour">
        <p><a href="{{route('index','#portfolio')}}"><button><i class="fas fa-arrow-left"></i>Retourner vers happyweb</button></a></p>
        <p><a href="https://alexanebellafionda.be/" target="_blank"><button>Visiter le <strong>site</strong></button></a></p>
    </div>
@endsection
