<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => "L':attribute doit être accepté.",
    'active_url' => "L':attribute n'est pas une URL active.",
    'after' => "L':attribute doit être une date après :date",
    'after_or_equal' => "L':attribute doit être une date après ou égal a :date",
    'alpha' => "L':attribute ne peut contenir que des lettres",
    'alpha_dash' => "L':attributes ne peut contenir que des lettres, chiffres, tirets",
    'alpha_num' => "L':attribute ne peut contenir que des lettres et des chiffres",
    'array' => "L':attribut doit être un tableau.",
    'before' => "L:attribute doit être une date avant :date.",
    'before_or_equal' => "L':attribute doit être une date avant ou égal à :date.",
    'between' => [
        'numeric' => "L':attribute doit être compris entre :min et :max",
        'file' => "L':attribute doit être compris entre :min et :max kilobytes.",
        'string' => "L':attributes doit contenir entre :min et :max caractères.",
        'array' => "L':attributes doit avoir entre :min et :max articles.",
    ],
    'boolean' => "Le champ :attriute doit être vrai ou faux.",
    'confirmed' => "La confirmation de :attribute ne correspond pas.",
    'date' => "Le champ :attribute n'est pas une date valide.",
    'date_equals' => "Le champ :attribute doit être égal à :date.",
    'date_format' => "Le champ :attribute ne correspond pas au format :format",
    'different' => "Le champ :attribute et :other doivent être différents.",
    'digits' => "Le champ :attributes doit être de :digits chiffres.",
    'digits_between' => "Le champ :attribute doit être compris entre :min et :max chiffres.",
    'dimensions' => "Le champ :attribute a une dimension d'image invalide.",
    'distinct' => "Le champ :attribute a une valeur dupliquée.",
    'email' => 'Le champ ":attribute" doit être une adresse email valide.',
    'ends_with' => "Le champ :attribute doit se finir avec l'une de ces valeurs :values",
    'exists' => "Le :attribute sélectionné est invalide.",
    'file' => "Le :attribute doit être un fichier.",
    'filled' => "Le champ :attribute doit avoir une valeur.",
    'gt' => [
        'numeric' => "Le attribute doit être plus grand que :value",
        'file' => "Le :attribute doit être plus grand que :value kilobytes.",
        'string' => "Le :attribute doit être plus grand que :value caractères.",
        'array' => "Le :attributes doit avoir plus que :value articles.",
    ],
    'gte' => [
        'numeric' => "Le :attribute doit être plus grand ou égal à :value.",
        'file' => "Le :attribute doit être plus grand ou égal à :value kilobytes.",
        'string' => "Le :attribute doit être plus grand ou égal à :value caractères. ",
        'array' => "Le :attribute doit avoir :value articles ou plus.",
    ],
    'image' => "Le :attribute doit être une image.",
    'in' => "Le :attribute sélectionné est inavalide.",
    'in_array' => "Le champ :attribute n'existe pas dan :other.",
    'integer' => "Le :attribute doit être un entier",
    'ip' => "Le :attribute doit être une adresse ip valide.",
    'ipv4' => "Le :attribute doit être une adresse IPv4 valide.",
    'ipv6' => "Le :attribute doit être une adresse IPv6 valide.",
    'json' => "L'attribut doit être une chaîne de caractères JSON valide.",
    'lt' => [
        'numeric' => "L'attribut doit être plus petit que :value.",
        'file' => "L'attribut doit être plus petit que :value kilobytes.",
        'string' => "L'attribut doit être plus petit que :value caractères.",
        'array' => "Le :attribute doit avoir moins que :value articles",
    ],
    'lte' => [
        'numeric' => "L'attriut doit être plus petit ou égal à :value.",
        'file' => "L'attriut doit être plus petit ou égal à :value kilobytes.",
        'string' => "L'attriut doit être plus petit ou égal à :value caratères.",
        'array' => "Le :attributes ne peut pas avoir plus de :value articles.",
    ],
    'max' => [
        'numeric' => "Le :attribute ne peut pas être plus grand que :max.",
        'file' => "Le :attribute ne peut pas être plus grand que :max kilobytes.",
        'string' => "Le :attribute ne peut pas être plus grand que :max caractères.",
        'array' => "Le :attribute ne peut pas avoir plus que :value items.",
    ],
    'mimes' => "Le :attribute doit être un fichier de type :values.",
    'mimetypes' => "Le :attribute doit être un fichier de type :values.",
    'min' => [
        'numeric' => "Le :attribute doit être d'au moins :min.",
        'file' => "Le :attribute doit être d'au moins :min kilobytes.",
        'string' => "Le :attribute doit être d'au moins :min caractères.",
        'array' => "Le :attriute doit au moins avoir :min articles.",
    ],
    'not_in' => "Le :attribute sélectionné est invalide.",
    'not_regex' => "Le format du :attribute n'est pas valide",
    'numeric' => "Le :attriute doit être un nombre.",
    'present' => "Le champ :attribute doit être présent.",
    'regex' => "Le format du :attribute n'est pas valide",
    'required' => 'Le champ ":attribute" est requis.',
    'required_if' => "Le champ :attribute est requis quand :other est :value.",
    'required_unless' => "Le champ :attribute est requis à moins que :other est dans :values.",
    'required_with' => "Le champ :attriute est requis quand :values est présent.",
    'required_with_all' => "Le champ :attriute est requis quand :values sont présentes.",
    'required_without' => "Le champ :attribute est requis quand :values n'est pas présent.",
    'required_without_all' => "Le champ :attribute est requis quand :values ne sont pas présentes.",
    'same' => "Le :attribute et :other doivent correspondre.",
    'size' => [
        'numeric' => "Le :attribute doit être de :size",
        'file' => "Le :attribute doit être de :size kilobytes",
        'string' => "Le :attribute doit être de :size caractères",
        'array' => "Le :attribute doit contenir :size articles.",
    ],
    'starts_with' => "Le :attribute doit commencer avec une de ces valeurs :values.",
    'string' => "Le :attributes doit être une chaîne de caractères.",
    'timezone' => "Le :attribute doit être une zone valide.",
    'unique' => "Le :attribute a déjà été pris.",
    'uploaded' => "Le chargement de :attribute a echoué.",
    'url' => "Le format de :attriute est invalide.",
    'uuid' => "Le :attribute doit être un valide UUID.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
