@extends('layouts.portfolioTemplate')

@section('content')
    <h2>Réalisation du curriculum vitae en ligne de David Houba</h2>
    <!-- Slideshow container -->
    <div class="slideshow-container">
        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade">
            <img src="{{asset('/images/portfolio/accueilHouba.png')}}" alt="Accueil Cv Houba" style="width:100%">
        </div>

        <div class="mySlides fade">
            <img src="{{asset('/images/portfolio/contactHouba.png')}}" alt="Contact Cv Houba" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/etudesHouba.png')}}" alt="Etudes Cv Houba" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/personnaliteHouba.png')}}" alt="Personnalité Cv Houba" style="width:100%">
        </div>

        <div class="mySlides fade">
            <img src="{{asset('/images/portfolio/portfolioHouba.png')}}" alt="Portfolio Cv Houba" style="width:100%">
        </div>

        <div class="mySlides fade">
            <img src="{{asset('/images/portfolio/galerieHouba.png')}}" alt="Galerie Cv Houba" style="width:100%">
        </div>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
    <div class="boutonRetour">
        <p><a href="{{route('index','#portfolio')}}"><button><i class="fas fa-arrow-left"></i>Retourner vers happyweb</button></a></p>
        <p><a href="https://davidhouba-dev.be/" target="_blank"><button>Visiter le <strong>site</strong></button></a></p>
    </div>
@endsection
