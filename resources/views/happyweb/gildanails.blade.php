@extends('layouts.portfolioTemplate')

@section('content')
    <h2>Réalisation du site de <strong>Gilda Nails</strong></h2>
    <!-- Slideshow container -->
    <div class="slideshow-container">
        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/gildaAccueil.jpg')}}" alt="Accueil Gilda Nails" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/gildaAcc.jpg')}}" alt="Accessoires Gilda Nails" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/gildaTar.jpg')}}" alt="Tarifs Gilda Nails" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/gildaConn.jpg')}}" alt="Connexion Gilda Nails" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/gildaAdm.jpg')}}" alt="Panel admin Gilda Nails" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/gildaModif.jpg')}}" alt="Modification Gilda Nails" style="width:100%">
        </div>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
    <div class="boutonRetour">
    <p><a href="{{route('index','#portfolio')}}"><button><i class="fas fa-arrow-left"></i>Retourner vers happyweb</button></a></p>
    <p><a href="https://www.gildanails.be/" target="_blank"><button>Visiter le <strong>site</strong></button></a></p>
</div>
@endsection
