<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/reset.css') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/images/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/images/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('/images/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('/images/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">
    <script src="https://kit.fontawesome.com/35d612b19a.js"></script>

    <title>Happy Web | Nos formules</title>
</head>
<body>

<div id="formulesClients_wrapper">

    <div class="head"><div class="logo"><a href="{{route('index','#formules')}}"><img src="{{ asset('/images/miniLogo.png') }}" alt="Logo"></a></div></div>
    <h2>Nos différentes formules</h2>

<div id="intro_formules">
    <p>Pour répondre et s'adapter au mieux à vos besoins, nous vous proposons différentes formules.</p>
</div>
    <div id="formules">

        <div id="formules_bronze">
            <div class="faceAvant card bronzeAvant">
                <div class="titreFormule"><h3>Formule Bronze :</h3></div>
                <h4>Details</h4>

                <p><strong>Création</strong> du produit</p>
                <p>Mise en ligne du produit</p>

                <h4>Maintenance (3 mois)</h4>

                <p><strong>Mise à jour</strong> des outils</p>
                <p><strong>Maintenance</strong> du <strong>site</strong></p>
                <p><strong>Support Technique</strong></p>
            </div>
            <div class="faceArriere card bronzeArriere">
                <p>Vous n'avez pas le temps de vous occuper de votre <strong>site</strong> ?</p>
                <p>Choisissez cette <strong>formule</strong> pour bien démarrer votre aventure !</p>
                <p><button class="btn" type="submit"><a href="{{route('index','#prenom')}}">Contactez-nous</a></button></p>
            </div>

        </div>

        <div id="formules_silver">
            <div class="faceAvant card silverAvant">
                <div class="titreFormule"><h3>Formule Silver :</h3></div>
                <h4>Details</h4>

                <p><strong>Création</strong> du produit</p>
                <p><strong>Mise en ligne</strong> du produit</p>

                <h4>Maintenance (6 mois)</h4>

                <p><strong>Mise à jour</strong> des outils</p>
                <p><strong>Maintenance</strong> du <strong>site</strong></p>
                <p><strong>Support Technique</strong></p>

                <h4>Modifications Mineures (6 mois)</h4>

                <p>Changement / Ajout photos</p>
                <p>Modification textes courts</p>
            </div>
            <div class="faceArriere card silverArriere">
                <p>Vous n'avez pas le temps de vous occuper de votre <strong>site</strong> ?</p>
                <p>Choisissez cette <strong>formule</strong> pour démarrer confortablement votre aventure !</p>
                <p><button class="btn" type="submit"><a href="{{route('index','#prenom')}}">Contactez-nous</a></button></p>
            </div>
        </div>

        <div id="formules_gold">
            <div class="faceAvant card goldAvant">
                <div class="titreFormule"><h3>Formule Gold :</h3></div>
                <h4>Details</h4>

                <p><strong>Création</strong> du produit</p>
                <p><strong>Mise en ligne</strong> du produit</p>

                <h4>Maintenance (1 an)</h4>

                <p><strong>Mise à jour</strong> des outils</p>
                <p><strong>Maintenance</strong> du <strong>site</strong></p>
                <p><strong>Support Technique</strong></p>

                <h4>Modifications Majeures (1 an)</h4>

                <p>Changement / Ajout photos</p>
                <p>Modification textes</p>
                <p>Légers changements du design</p>
            </div>
            <div class="faceArriere card goldArriere">
                <p>Vous n'avez pas le temps de vous occuper de votre <strong>site</strong> ?</p>
                <p>Choisissez cette <strong>formule</strong> pour démarrer votre aventure et atteignez les sommets ! </p>
                <p><button class="btn" type="submit"><a href="{{route('index','#prenom')}}">Contactez-nous</a></button></p>
            </div>
        </div>
    </div>


    <div id="conclusion_formule">
        <p>Il vous reste encore des questions ?
            N’hésitez pas à <a href="{{route('index','#prenom')}}">nous contacter</a>, nous y répondrons avec plaisir ! ;-) </p>
    </div>
    <div class="boutonRetour">
        <p><a href="{{route('index','#portfolio')}}"><button class="retour"><i class="fas fa-arrow-left"></i>Retourner vers happyweb</button></a></p>
    </div>

    @include('layouts/partials/_footer')
</div>
<script src="{{ asset('/js/cardFormule.js') }}"></script>
</body>
</html>

