@extends('layouts.portfolioTemplate')

@section('content')
    <h2>Réalisation d'un site internet pour l'entreprise <strong>Mdcarre</strong></h2>
    <!-- Slideshow container -->
    <div class="slideshow-container">
        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade">
            <img src="{{asset('/images/portfolio/mdcarreReal.png')}}" alt="Réalisation de a à z MdCarre" style="width:100%">
        </div>

        <div class="mySlides fade">
            <img src="{{asset('/images/portfolio/mdcarreQuiSommesNous.png')}}" alt="Qui sommes nous ? MdCarre" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/mdcarreModifContact.png')}}" alt="Modifications contact MdCarre" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/mdcarreModifPass.png')}}" alt="Modification mot de passe MdCarre" style="width:100%">
        </div>

        <div class="mySlides fade">
            <img src="{{asset('/images/portfolio/mdcarreModif.png')}}" alt="Modifications MdCarre" style="width:100%">
        </div>

        <div class="mySlides fade">
            <img src="{{asset('/images/portfolio/mdcarrePanelAdmin.png')}}" alt="Panel admin MdCarre" style="width:100%">
        </div>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
    <div class="boutonRetour">
        <p><a href="{{route('index','#portfolio')}}"><button><i class="fas fa-arrow-left"></i>Retourner vers happyweb</button></a></p>
        <p><a href="https://www.mdcarre.be/" target="_blank"><button>Visiter le <strong>site</strong></button></a></p>
    </div>
@endsection
