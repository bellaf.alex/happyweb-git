@extends('layouts.portfolioTemplate')

@section('content')
    <h2>Réalisation du site du <strong>Dr.E Houyoux</strong></h2>
    <!-- Slideshow container -->
    <div class="slideshow-container">
        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/accueilHouyoux.jpg')}}" alt="Accueil Houyoux" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/mesoHouyoux.jpg')}}" alt="Mésothérapie" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/ridesHouyoux.jpg')}}" alt="Comblement des rides" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/tabHouyoux.jpg')}}" alt="Tableau des prix" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/microHouyoux.jpg')}}" alt="Microdermabrasion" style="width:100%">
        </div>

        <div class="mySlides fade">
        <img src="{{asset('/images/portfolio/prpHouyoux.jpg')}}" alt="Plasma riche en plaquettes" style="width:100%">
        </div>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
    <div class="boutonRetour">
        <p><a href="{{route('index','#portfolio')}}"><button><i class="fas fa-arrow-left"></i>Retourner vers happyweb</button></a></p>
        <p><a href="http://www.medecine-esthetique-houyoux.be/" target="_blank"><button>Visiter le <strong>site</strong></button></a></p>
    </div>
@endsection
