<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class ContactRequest extends formRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prenom'=>'required|min:3',
            'email'=>'required|email',
            'tel'=>'required',
            'sujetList'=>'required',
            'sujet'=>'required|min:10',
        ];
    }
}
